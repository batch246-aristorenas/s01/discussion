# [Section] Comments
# Comments in Python are done using the "#" symbol

# Display output in Python
print("Hello World")

# [Section] Indentions
# Where in other programming languages the indentiion in code is for readability only, the indention in Python is very important

# [Section] Variables
# Recall the use of variables which are containers of data
# In Python, a variable is declared by stating the variable name and assigning a value using the equality symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with a letter (A to Z, a to z), dollar sign($) or an underscore
# After the first character, identifiers can have combination of characters
# Most importantly, identifiers are case sensitive
age = 35
middle_initial = "C"

# Python allows assigning of values to multiple variables in one line:
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# [Section] Data Types

# 1. String
# 1. Strings (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers (int, float, complex) - for integers, decimals and complex numbers

#int
num_of_days = 4

#float
pi_appox = 3.1416 

#complex number, 'j' represents the imaginary complex
complex_num = 1 + 5j

# 3. Boolean - for truth values
# Boolean values in Python start with uppercase letters
isLearning = True
isDifficult = False


# [Section] Using Variables

# To use variables, concatenation (+ symbol) between strings can be used
print("My name is " + full_name)

# In Python, outputing in the terminal uses the print() function
print("Hello World again")

# print("My age is " + age)
# This returns a type error as numbers can't be concatenated to strings
# To solve this, typecasting can be used
print("My age is " + str(age) + ".")


# [Section] Typecasting - changes data type
# 1. int() - converts value into an integer value
# 2. float() - converts value into an float value
# 3. str() - converts value into string
print(int(3.5))

# same output but different data type
print("404") # string
print(int("404")) # integer

# Another way to avoid type error in printing without the use of typecasting is the use of F-strings
# To use F-strings, add a lowercase "f" before the string and place the desired variable in {}
print(f"Myname is {full_name} and my age is {age}.")


# [Section] Operations
#Arithmetic Operators
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4) # modulo - finding remainder
print(2 ** 3) # exponent

# Assignment Operators - ised to assign values to variables
num1 = 3 # 3
num1 += 4 # adds 4 from current value, new value:
print(num1)


#Comparison operators (==) - used to compare values (it returns a boolean value - true or false)
print(1 == 1)

print("------------")
# Logical Operators  - used to combine conditional statements
print(True and False and True)
print(not False)
print(False or True)
